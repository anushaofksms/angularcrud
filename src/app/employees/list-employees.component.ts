import { Component, OnInit } from '@angular/core';
import { Employee } from './../models/employee.model';
import { EmployeeService } from './employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ResolvedEmployeeList } from './resolved-employeelist.model';


@Component({
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css']
})
export class ListEmployeesComponent implements OnInit {
  
  // searchTerm: string; 
  filteredEmployees: Employee[];
  error:string;
  employees: Employee[];
  dataFromChild:Employee;
  private _searchTerm: string;

  get searchTerm(): string {
    return this._searchTerm;
  }

  set searchTerm(value: string) {
    this._searchTerm = value;
    this.filteredEmployees = this.filterEmployees(value);
  }

  filterEmployees(searchString: string) {
    return this.employees.filter(employee =>
      employee.name.toLowerCase().indexOf(searchString.toLowerCase()) !== -1);
  }


  constructor(private _employeeService: EmployeeService,
    private _router: Router,
    private _route: ActivatedRoute) {

    const resolvedEmployeeList:Employee[] | string = this.employees = this._route.snapshot.data['employeeList'];
    
    if(Array.isArray(resolvedEmployeeList))
    {
       this.employees = resolvedEmployeeList;
    }
    else
    {
      this.error= resolvedEmployeeList;
    }

    if (this._route.snapshot.queryParamMap.has('searchTerm')) {
      this.searchTerm = this._route.snapshot.queryParamMap.get('searchTerm');
    } else {
      this.filteredEmployees = this.employees;
    }
  }

  ngOnInit() {
  }

  notifyHandle(eventData: Employee)
  {
    this.dataFromChild = eventData;
  }

  onDeeleteNotification(id:number)
  {
    const i = this.filteredEmployees.findIndex(e => e.id === id);
    if (i !== -1) {
      this.filteredEmployees.splice(i, 1);
    }
  }
}