import { Injectable } from '@angular/core';
import { Employee } from '../models/employee.model';

import { Observable } from 'rxjs/Observable';
import { HttpClient,HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { catchError } from 'rxjs/operators';

@Injectable()

export class EmployeeService {
    
    constructor(private httpClient: HttpClient) {
    }

    private listEmployees: Employee[] = [
        {
            id: 1,
            name: 'Mark',
            gender: 'Male',
            contactPreference: 'Email',
            email: 'mark@pragimtech.com',
            dateOfBirth: new Date('10/25/1988'),
            department: 'IT',
            isActive: true,
            photoPath: 'assets/images/mark.png'
        },
        {
            id: 2,
            name: 'Mary',
            gender: 'Female',
            contactPreference: 'Phone',
            phoneNumber: 2345978640,
            dateOfBirth: new Date('11/20/1979'),
            department: 'HR',
            isActive: true,
            photoPath: 'assets/images/sexy.jpg'
        },
        {
            id: 3,
            name: 'John',
            gender: 'Male',
            contactPreference: 'Phone',
            phoneNumber: 5432978640,
            dateOfBirth: new Date('3/25/1976'),
            department: 'IT',
            isActive: false,
            photoPath: 'assets/images/john.png'
        },
    ];

    getEmployees(): Observable<Employee[]> {
        return this.httpClient.get<Employee[]>('http://localhost:3000/employees');
      }

    // private handleError(errorResponse: HttpErrorResponse) {
    //     if (errorResponse.error instanceof ErrorEvent) {
    //         console.error('Client Side Error :', errorResponse.error.message);
    //     } else {
    //         console.error('Server Side Error :', errorResponse);
    //     }
    //     // return an observable with a meaningful error message to the end user
    //     return new ErrorObservable("There is a problem with the service.");
    // }
    
    getEmployee(id: number): Employee {
        return this.listEmployees.find(e => e.id === id);
    } 

    save(employee: Employee) {
       if(employee.id === null)
       {
           return this.httpClient.post('http://localhost:3000/employees',  employee, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        })
           this.listEmployees.push(employee);
       }
       else
       {
           const foundIndex = this.listEmployees.findIndex(e=> e.id === employee.id);
           this.listEmployees[foundIndex] = employee;
       }
    } 

    deleteEmployee(id: number) {
        const i = this.listEmployees.findIndex(e => e.id === id);
        if (i !== -1) {
          this.listEmployees.splice(i, 1);
        }
      }
}